#! /bin/sh

set -e

JAR_FILE=$1


# #################################
# Detect environment
# #################################
if [ "${ECS_WITH_CONSUL}" == "yes" ]; then
  echo "==> Discovering container host IP and port to register with Consul"
  # Check if script is executing in an AWS instance
  if curl -s -q http://169.254.169.254 --connect-timeout 0.5 > /dev/null; then
    export SPRING_CLOUD_CONSUL_DISCOVERY_IP_ADDRESS=$( curl -s http://169.254.169.254/latest/meta-data/local-ipv4/ )

    if [ ! -z "${SPRING_CLOUD_CONSUL_DISCOVERY_IP_ADDRESS}" ]; then
      echo "==> Found container host IP: ${SPRING_CLOUD_CONSUL_DISCOVERY_IP_ADDRESS}"

      # Wait until container metadata is complete
      until grep "ContainerID" $ECS_CONTAINER_METADATA_FILE > /dev/null; do sleep 1; done

      export SPRING_CLOUD_CONSUL_DISCOVERY_PORT=$( cat $ECS_CONTAINER_METADATA_FILE | jq -r ".PortMappings[] .HostPort" )

      if [ ! -z "${SPRING_CLOUD_CONSUL_DISCOVERY_PORT}" ]; then
        echo "==# Could not detect container host port. Failing!"
        exit 1
      else
        echo "==> Found container host port: ${SPRING_CLOUD_CONSUL_DISCOVERY_PORT}"
      fi
    else
      echo "==# Could not detect container host IP. Failing!"
      exit 1
    fi
  fi
fi

# #################################
# Execute
# #################################
case ${JAR_FILE} in
  *.jar) isJarFile="Yes" ;;
esac

if [ ! -z "${APP_WORK_DIR}" ]; then
  cd ${APP_WORK_DIR}
fi

if [ -z "${JAR_FILE}" ] || [ -z "${isJarFile}" ]; then
  exec $@
else
  exec /opt/java/openjdk/bin/java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar $@
fi
