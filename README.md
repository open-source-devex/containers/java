# Docker container to run Java apps

The container is build on top of the (adoptopenjdk/openjdk8-openj9)[https://hub.docker.com/r/adoptopenjdk/openjdk8-openj9/] alpine based image.

## Use

### Run the container

This container can be used to run java processes as is, just mount a volume with a jar to run and provide the jar path as the argument to `docker run`.
As environment variables the container accepts:
* `JAVA_OPTS` for the options to pass to the `java` command (default: "").
* `APP_WORK_DIR` which the entry-point script will `cd` to when set (default: "").
* `ECS_WITH_CONSUL` which when set to "yes" triggers the entry-point script to try to detect usable IP and PORT values for discovery with consul (default: "no").

To run a JAR inside the container
```bash
docker run \
  -v /path/to/my.jar:/my.jar \
  -e JAVA_OPTS="-Xmx256m" \
  -e APP_WORK_DIR="/tmp" \
  registry.gitlab.com/open-source-devex/containers/java:latest \
  /my.jar
```

To get a shell
```bash
docker run -it registry.gitlab.com/open-source-devex/containers/java:latest ash
```

By default, the container executes under user `bender` but that can be changed at runtime.

### Extend the container

This container can be extended in new `Dockerfile`.

To extend this container to run a packaged JAR
```Dockerfile
FROM registry.gitlab.com/open-source-devex/containers/java:latest

# MORE DOCKER INSTRUCTIONS
# ...

ADD my.jar /home/bender/my.jar

ENV JAVA_OPTS="-Xmx512m"
ENV APP_WORK_DIR="/home/bender"
ENV ECS_WITH_CONSUL="no" # set to yes if running on ECS with Consul

EXPOSE 8080

CMD [ "/home/bender/my.jar" ]

```

## Changes to this container

This container is automatically tested at every change. The testing happens in the `run_tests.sh` script.
Any change to the container should be covered by either adapting the existing tests or adding new ones.
