#!/usr/bin/env sh

set -e

if [ -f ".env" ]; then
  source .env
fi

VERSION=$( cat .version )

git tag --annotate v${VERSION} --message "Release v${VERSION}"
git push --tags

if [ -z "$( git branch | grep '^* master$' )" ]; then
  # Checkout master
  git branch -d master
  git fetch origin master:master
  git checkout master
fi
