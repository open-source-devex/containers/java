#!/usr/bin/env sh

set -e

if [ -f ".env" ]; then
  source .env
fi

set -v

if [ -z "${CONTAINER_TEST_IMAGE}" ]; then
  CONTAINER_TEST_IMAGE=test-image
fi

if [ -z "${CONTAINER_RELEASE_IMAGE}" ]; then
  CONTAINER_RELEASE_IMAGE=release-image
fi

docker pull ${CONTAINER_TEST_IMAGE}

JDK_VERSION=$( docker run --rm -t ${CONTAINER_TEST_IMAGE} /opt/java/openjdk/bin/java -version | grep 'OpenJDK Runtime Environment' | sed -e 's/OpenJDK.* (build \(.*\))/\1/' | sed -e 's/+/-/' | tr -d '[:space:]' )
JVM_VERSION=$( docker run --rm -t ${CONTAINER_TEST_IMAGE} /opt/java/openjdk/bin/java -version | grep 'Eclipse OpenJ9 VM' | sed -e 's/Eclipse.*(build \(.*\), JRE.*)/\1/' | tr -d '[:space:]' )
VERSION=${JDK_VERSION}_${JVM_VERSION}

echo ${VERSION} > .version

docker tag ${CONTAINER_TEST_IMAGE} ${CONTAINER_RELEASE_IMAGE}:${VERSION}
docker push ${CONTAINER_RELEASE_IMAGE}:${VERSION}

docker tag ${CONTAINER_TEST_IMAGE} ${CONTAINER_RELEASE_IMAGE}:${VERSION}-latest
docker push ${CONTAINER_RELEASE_IMAGE}:${VERSION}-latest
